angular.module('ClearEnergy')
  .factory('Cart', function CartFactory() {
    var factory = this;

    if ( localStorage.getItem('cart') ) {
      factory.cart = getCart();
    } else {
      factory.cart = [];
      localStorage.setItem('cart', '[]');
    }

    function getCart() {
      var storage = localStorage.getItem('cart');

      return JSON.parse(storage);
    }

    function saveCart(cart) {
      var cartJson = JSON.stringify(cart);
      localStorage.setItem('cart', cartJson);

      return true;
    }

    return {
      get: function() {
        return factory.cart;
      },
      add: function(product) {
        factory.cart.push(product);
        saveCart(factory.cart);
      },
      remove: function(product) {
        var productIndex = factory.cart.indexOf(product);
        factory.cart.splice(productIndex, 1);
        saveCart(factory.cart);
      },
      isEmpty: function() {
        return factory.cart.length == 0;
      },
      purchase: function() {
        factory.cart = [];
        localStorage.removeItem('cart');
      }
    }
  });
