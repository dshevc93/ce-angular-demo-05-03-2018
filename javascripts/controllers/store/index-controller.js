angular.module('ClearEnergy')
  .controller('StoreIndexController', function($scope, $http, Cart) {
    $http.get('/products.json').then(function(response) {
      $scope.products = response.data;
    });

    $scope.addToCart = function(product) {
      Cart.add(product);
    }
  });
