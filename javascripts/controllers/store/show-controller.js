angular.module('ClearEnergy')
  .controller('StoreShowController', function($scope, $http, $routeParams, Cart) {
    $http.get('/products.json').then(function(response) {
      var products = response.data;
      var productToShow = products.find(function(product) { return product.id == $routeParams.id })
      $scope.product = productToShow
    });

    $scope.addToCart = function() {
      Cart.add($scope.product);
    }
  });
