angular.module('ClearEnergy')
  .controller('CartIndexController', function($scope, Cart) {
    $scope.products = Cart.get();

    $scope.removeProduct = function(product, element) {
      Cart.remove(product);
    }

    $scope.cartIsEmpty = function() {
      return Cart.isEmpty();
    }

    $scope.totalPrice = function() {
      var totalPrice = 0;
      var productsQuantity = $scope.products.length;

      for(var index = productsQuantity - 1; index >= 0; index--) {
        totalPrice += Number($scope.products[index].price);
      }

      return totalPrice;
    }

    $scope.finishOrder = function() {
      Cart.purchase()
    }
  })
