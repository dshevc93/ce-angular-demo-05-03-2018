angular.module('ClearEnergy')
  .filter('capitalize', function () {
    return function (item) {
      item = item.toLowerCase();
      return item.substring(0,1).toUpperCase()+item.substring(1);
    };
  });
