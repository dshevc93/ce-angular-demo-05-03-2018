angular.module('ClearEnergy')
  .directive('filterType', function() {
    return {
      restrict: 'E',
      templateUrl: 'templates/directives/filter-type.html',
      scope: {
        type: '@'
      },
      require: '^productsFilter',
      link: function(scope, element, attributes, productsFilter) {
        scope.setAsCurrentFilter = function() {
          productsFilter.setFilter(scope.type);
        }

        scope.isActive = function() {
          return productsFilter.currentFilter() === scope.type;
        }
      }
    }
  });
