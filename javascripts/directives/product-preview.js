angular.module('ClearEnergy')
  .directive('productPreview', function() {
    return {
      restrict: 'E',
      templateUrl: 'templates/directives/product-preview.html',
    }
  });
