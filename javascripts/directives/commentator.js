angular.module('ClearEnergy')
  .directive('commentator', function() {
    return {
      restrict: 'A',
      link: function(scope, element, attributes) {
        var targetLink = element[0];

        targetLink.addEventListener('click', function() {
          var commentator = document.createElement('span');
          commentator.className = 'commentator';
          commentator.textContent = 'Item added to your cart!';

          targetLink.parentElement.insertBefore(commentator, targetLink);

          setTimeout(function() {
            document.querySelector('.commentator').remove()
          }, 1600);
        });
      }
    }
  })
