angular.module('ClearEnergy')
  .directive('productsFilter', function() {
    return {
      restrict: 'E',
      templateUrl: 'templates/directives/products-filter.html',
      controller: function($scope) {
        this.setFilter = function(type) {
          $scope.currentFilter = type;
        }

        this.currentFilter = function() {
          return $scope.currentFilter;
        }

        $scope.resetFilter = function() {
          $scope.currentFilter = '';
        }
      }
    }
  });
