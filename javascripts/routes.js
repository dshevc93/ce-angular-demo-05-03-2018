angular.module('ClearEnergy')
  .config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
  }])
  .config(function($routeProvider) {
    $routeProvider.when('/store', {
      templateUrl: '../templates/pages/store/index.html'
    })
    .when('/products/:id', {
      templateUrl: '../templates/pages/store/show.html',
      controller: 'StoreShowController'
    })
    .when('/cart', {
      templateUrl: '../templates/pages/cart/index.html',
      controller: 'CartIndexController'
    })
    .when('/account', {
      templateUrl: '../templates/pages/account/index.html'
    })
    .when('/', {
      templateUrl: '../templates/pages/store/index.html'
    })
    .otherwise({
      redirectTo: '/'
    })
  });
